/*
 * This work is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License (http://creativecommons.org/licenses/by-sa/3.0/)
 */
package com.jeremyvinding.AntiSocialMedia;

import android.app.Application;
import android.os.StrictMode;

public class AntiSocialMediaApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        final StrictMode.ThreadPolicy.Builder builder = new StrictMode.ThreadPolicy.Builder()
                .detectDiskReads()
                .detectDiskWrites()
                .detectNetwork()
                .penaltyLog();

        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
            builder.penaltyDialog();
        } else {
            builder.penaltyFlashScreen();

        }
        StrictMode.setThreadPolicy(builder.build());
    }
}
