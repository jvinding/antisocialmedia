/*
 * This work is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License (http://creativecommons.org/licenses/by-sa/3.0/)
 */
package com.jeremyvinding.AntiSocialMedia.activities;

import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import com.jeremyvinding.AntiSocialMedia.R;

/* package */ abstract class BaseActivity extends Activity {
    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        final MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onMenuItemSelected(final int featureId, final MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_compose:
                startActivity(new Intent(this, WritePostActivity.class));
                return true;
        }
        return super.onMenuItemSelected(featureId, item);
    }
}
