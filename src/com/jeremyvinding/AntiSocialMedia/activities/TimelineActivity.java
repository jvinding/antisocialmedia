/*
 * This work is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License (http://creativecommons.org/licenses/by-sa/3.0/)
 */
package com.jeremyvinding.AntiSocialMedia.activities;

import android.content.Intent;
import android.os.Bundle;
import com.jeremyvinding.AntiSocialMedia.R;
import com.jeremyvinding.AntiSocialMedia.providers.AntiSocialMediaContract;
import com.jeremyvinding.AntiSocialMedia.services.TimelineService;
import com.jeremyvinding.AntiSocialMedia.timeline.TimelineFragment;

public class TimelineActivity extends BaseActivity {
    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.timeline);

        final TimelineFragment fragment = (TimelineFragment) getFragmentManager().findFragmentById(R.id.timeline_fragment);
        fragment.setRowLayout(R.layout.timeline_post_list_item, POST_PROJECTION);
        fragment.loadTimeline();

    }

    @Override
    protected void onResume() {
        super.onResume();
        startService(new Intent(this, TimelineService.class));
    }

    // privates
    private static final String[] POST_PROJECTION = new String[]{
            AntiSocialMediaContract.Post._ID,
            AntiSocialMediaContract.Post.COLUMN_NAME_USER_ID,
            AntiSocialMediaContract.User.COLUMN_NAME_USER_NAME,
            AntiSocialMediaContract.User.COLUMN_NAME_DISPLAY_NAME,
            AntiSocialMediaContract.User.COLUMN_NAME_AVATAR_URL,
            AntiSocialMediaContract.Post.COLUMN_NAME_TIMESTAMP,
            AntiSocialMediaContract.Post.COLUMN_NAME_TEXT
    };
}