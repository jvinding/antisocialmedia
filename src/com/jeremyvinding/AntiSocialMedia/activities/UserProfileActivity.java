/*
 * This work is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License (http://creativecommons.org/licenses/by-sa/3.0/)
 */
package com.jeremyvinding.AntiSocialMedia.activities;

import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import com.jeremyvinding.AntiSocialMedia.R;
import com.jeremyvinding.AntiSocialMedia.providers.AntiSocialMediaContract;
import com.jeremyvinding.AntiSocialMedia.timeline.TimelineFragment;
import com.jeremyvinding.AntiSocialMedia.util.ImageLoader;

public class UserProfileActivity extends BaseActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    public static final String INTENT_KEY_USER_ID = "user_id";

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_profile);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        mPostCountView = (TextView) findViewById(R.id.post_count);
        mNameView = (TextView) findViewById(R.id.user_name);
        mDisplayNameView = (TextView) findViewById(R.id.user_display_name);
        mAvatarView = (ImageView) findViewById(R.id.user_avatar);

        parseIntent(getIntent());

        final TimelineFragment fragment = (TimelineFragment) getFragmentManager().findFragmentById(R.id
                                                                                                           .timeline_fragment);
        fragment.setUserId(mUserId);
        fragment.setRowLayout(R.layout.user_post_list_item, POST_PROJECTION);
        fragment.loadTimeline();

        getLoaderManager().initLoader(LOADER_USER, null, this);
        getLoaderManager().initLoader(LOADER_POST_COUNT, null, this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(final int id, final Bundle args) {
        final Loader<Cursor> loader;
        if (LOADER_USER == id) {
            final Uri uri = AntiSocialMediaContract.User.buildUserUri(mUserId);
            loader = new CursorLoader(this, uri, USER_PROJECTION, null, null, null);
        } else if (LOADER_POST_COUNT == id) {
            final Uri uri = AntiSocialMediaContract.User.buildPostsUri(mUserId);
            loader = new CursorLoader(this, uri, new String[]{AntiSocialMediaContract.Post._COUNT}, null, null, null);
        } else {
            loader = null;
        }
        return loader;
    }

    @Override
    public void onLoadFinished(final Loader<Cursor> loader, final Cursor data) {
        final int id = loader.getId();
        if (LOADER_USER == id) {
            setupUserView(data);
        } else if (LOADER_POST_COUNT == id) {
            updatePostCount(data);
        }
    }

    @Override
    public void onLoaderReset(final Loader<Cursor> loader) {
        final int id = loader.getId();
        if (LOADER_USER == id) {
            setupUserView(null);
        } else if (LOADER_POST_COUNT == id) {
            updatePostCount(null);
        }
    }

    private void updatePostCount(final Cursor cursor) {
        int count = 0;
        if (null != cursor) {
            cursor.moveToFirst();
            count = cursor.getInt(cursor.getColumnIndex(AntiSocialMediaContract.User._COUNT));
        }
        mPostCountView.setText(getResources().getQuantityString(R.plurals.user_post_count, count, count));
    }

    /*
     * Privates
     */
    private static final int LOADER_USER = 1;
    private static final int LOADER_POST_COUNT = 2;

    private static final String[] USER_PROJECTION = new String[]{
            AntiSocialMediaContract.User._ID,
            AntiSocialMediaContract.User.COLUMN_NAME_USER_NAME,
            AntiSocialMediaContract.User.COLUMN_NAME_DISPLAY_NAME,
            AntiSocialMediaContract.User.COLUMN_NAME_AVATAR_URL
    };

    private static final String[] POST_PROJECTION = new String[]{
            AntiSocialMediaContract.Post._ID,
            AntiSocialMediaContract.Post.COLUMN_NAME_TIMESTAMP,
            AntiSocialMediaContract.Post.COLUMN_NAME_TEXT
    };

    private long mUserId;
    private TextView mPostCountView;
    private TextView mDisplayNameView;
    private TextView mNameView;
    private ImageView mAvatarView;

    private void parseIntent(final Intent intent) {
        mUserId = intent.getLongExtra(INTENT_KEY_USER_ID, -1L);
    }

    private void setupUserView(final Cursor data) {
        if (null == data) {
            mNameView.setText("");
            mDisplayNameView.setText("");
            mAvatarView.setImageResource(R.drawable.silhouette);
        } else {
            data.moveToFirst();
            mNameView.setText(String.format("@%s", data.getString(data.getColumnIndex(
                    AntiSocialMediaContract.User.COLUMN_NAME_USER_NAME))));
            mDisplayNameView.setText(data.getString(data.getColumnIndex(
                    AntiSocialMediaContract.User.COLUMN_NAME_DISPLAY_NAME)));
            final int index = data.getColumnIndex(AntiSocialMediaContract.User.COLUMN_NAME_AVATAR_URL);
            final String avatarUrl = data.getString(index);
            new ImageLoader(mAvatarView, R.drawable.silhouette).execute(avatarUrl);
        }
    }
}