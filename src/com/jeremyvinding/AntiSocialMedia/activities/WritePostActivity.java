/*
 * This work is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License (http://creativecommons.org/licenses/by-sa/3.0/)
 */
package com.jeremyvinding.AntiSocialMedia.activities;

import android.app.Activity;
import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import com.jeremyvinding.AntiSocialMedia.R;

public class WritePostActivity extends Activity {

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.writepost);
        mSoundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
        mAudioManager = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
        mSound = mSoundPool.load(this, R.raw.cricket, 1);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        final MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.post, menu);
        return true;
    }

    @Override
    public boolean onMenuItemSelected(final int featureId, final MenuItem item) {
        if (R.id.menu_send == item.getItemId()) {
            submitPost();
        }
        return super.onMenuItemSelected(featureId, item);
    }

    private void submitPost() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        playSound();
                        finish();
                    }
                });
            }
        }).start();
    }

    private SoundPool mSoundPool;
    private AudioManager mAudioManager;
    private int mSound;

    private void playSound() {
        float streamVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        streamVolume = streamVolume / mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        mSoundPool.play(mSound, streamVolume, streamVolume, 1, 1, 1f);
    }
}