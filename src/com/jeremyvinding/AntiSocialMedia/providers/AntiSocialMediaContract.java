/*
 * This work is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License (http://creativecommons.org/licenses/by-sa/3.0/)
 */
package com.jeremyvinding.AntiSocialMedia.providers;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

public final class AntiSocialMediaContract  {
    public static final String AUTHORITY = "com.jeremyvinding.provider.timeline";
    private static final Uri BASE_CONTENT_URI = Uri.parse("content://" + AUTHORITY);

    public static final class User implements BaseColumns {
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath("users").build();

        public static final String CONTENT_TYPE_LIST = "vnd.android.cursor.dir/vnd.jeremyvinding.user";
        public static final String CONTENT_TYPE_ITEM = "vnd.android.cursor.item/vnd.jeremyvinding.user";

        public static final String TABLE_NAME = "user";
        public static final String COLUMN_NAME_USER_NAME = "name";
        public static final String COLUMN_NAME_DISPLAY_NAME = "display_name";
        public static final String COLUMN_NAME_AVATAR_URL = "avatar_url";

        public static Uri buildUserUri(final long userId) {
            return ContentUris.withAppendedId(CONTENT_URI, userId);
        }
        
        public static Uri buildPostsUri(final long userId) {
            return CONTENT_URI.buildUpon().appendPath(String.valueOf(userId)).appendPath("posts").build();
        }

        private User() {}
    }

    public static final class Post implements BaseColumns {
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath("posts").build();

        public static final String CONTENT_TYPE_LIST = "vnd.android.cursor.dir/vnd.jeremyvinding.post";
        public static final String CONTENT_TYPE_ITEM = "vnd.android.cursor.item/vnd.jeremyvinding.post";

        public static final String TABLE_NAME = "post";
        public static final String COLUMN_NAME_USER_ID = "user_id";
        public static final String COLUMN_NAME_TEXT = "text";
        public static final String COLUMN_NAME_TIMESTAMP = "timestamp";

        public static Uri buildPostUri(final long postId) {
            return ContentUris.withAppendedId(CONTENT_URI, postId);
        }

        private Post() {}
    }

    private AntiSocialMediaContract() {}
}
