/*
 * This work is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License (http://creativecommons.org/licenses/by-sa/3.0/)
 */
package com.jeremyvinding.AntiSocialMedia.providers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static com.jeremyvinding.android.common.LogUtils.LOGI;

/* package */ class AntiSocialMediaDBHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;

    public AntiSocialMediaDBHelper(final Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(final SQLiteDatabase db) {
        for (final String sql : SQL_CREATE_DB) {
            db.execSQL(sql);
        }
    }

    @Override
    public void onUpgrade(final SQLiteDatabase db, final int oldVersion, final int newVersion) {
        LOGI(TAG, String.format("upgrading db from %d to %d", oldVersion, newVersion));
        db.execSQL("DROP TABLE IF EXISTS " + AntiSocialMediaContract.Post.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + AntiSocialMediaContract.User.TABLE_NAME);
        onCreate(db);

    }

    // most privates
    private static final String DATABASE_NAME = "Feed.db";
    private static final String TAG = AntiSocialMediaDBHelper.class.getSimpleName();

    private static final String[] SQL_CREATE_DB = {
            "CREATE TABLE " + AntiSocialMediaContract.User.TABLE_NAME + " (" +
                    AntiSocialMediaContract.User._ID + " INTEGER PRIMARY KEY," +
                    AntiSocialMediaContract.User.COLUMN_NAME_USER_NAME + " TEXT UNIQUE NOT NULL," +
                    AntiSocialMediaContract.User.COLUMN_NAME_DISPLAY_NAME + " TEXT NOT NULL," +
                    AntiSocialMediaContract.User.COLUMN_NAME_AVATAR_URL + " TEXT" +
                    ")",
            "CREATE TABLE " + AntiSocialMediaContract.Post.TABLE_NAME + " (" +
                    AntiSocialMediaContract.Post._ID + " INTEGER PRIMARY KEY," +
                    AntiSocialMediaContract.Post.COLUMN_NAME_USER_ID + " INTEGER NOT NULL," +
                    AntiSocialMediaContract.Post.COLUMN_NAME_TEXT + " TEXT NOT NULL," +
                    AntiSocialMediaContract.Post.COLUMN_NAME_TIMESTAMP + " INTEGER" +
                    ")",
            "CREATE INDEX user_id_idx ON " + AntiSocialMediaContract.Post.TABLE_NAME + "(user_id)"
    };
}
