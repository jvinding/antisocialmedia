/*
 * This work is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License (http://creativecommons.org/licenses/by-sa/3.0/)
 */
package com.jeremyvinding.AntiSocialMedia.providers;

import android.content.*;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.text.TextUtils;
import com.jeremyvinding.android.common.SelectionBuilder;

import java.util.ArrayList;

public class AntiSocialMediaProvider extends ContentProvider {
    public static final String ORDER_BY_MOST_RECENT_FIRST = AntiSocialMediaContract.Post.COLUMN_NAME_TIMESTAMP + " " +
            "DESC, " + AntiSocialMediaContract.Post.TABLE_NAME + "." + AntiSocialMediaContract.Post._ID + " DESC";

    @Override
    public boolean onCreate() {
        mDBHelper = new AntiSocialMediaDBHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(final Uri uri, final String[] columns, final String selection,
                        final String[] selectionArgs, final String sortOrder) {
        SQLiteDatabase db;
        try {
            db = mDBHelper.getWritableDatabase();
        } catch (final SQLiteException e) {
            db = mDBHelper.getReadableDatabase();
        }

        final SelectionBuilder selectionBuilder = createSelectionBuilder(uri);

        final int type = URI_MATCHER.match(uri);
        switch (type) {
            case USER_ID:
                selectionBuilder.where(AntiSocialMediaContract.User._ID + "=?", parseUserId(uri));
                break;

            case USER_ID_POSTS:
                selectionBuilder
                        .map(AntiSocialMediaContract.Post._COUNT, "count(*)")
                        .where(AntiSocialMediaContract.Post.COLUMN_NAME_USER_ID + "=?", parseUserId(uri));
                break;

            case POST_ID:
                selectionBuilder.where(AntiSocialMediaContract.Post._ID + "=?", parsePostId(uri));
                break;

            case USERS:
            case POSTS:
                // nothing more to do here
                break;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }

        selectionBuilder.where(selection, selectionArgs);

        final Cursor cursor = selectionBuilder.query(db, columns, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Override
    public String getType(final Uri uri) {
        final String type;
        switch (URI_MATCHER.match(uri)) {
            case USER_ID:
                type = AntiSocialMediaContract.User.CONTENT_TYPE_ITEM;
                break;
            case USERS:
                type = AntiSocialMediaContract.User.CONTENT_TYPE_LIST;
                break;
            case POST_ID:
                type = AntiSocialMediaContract.Post.CONTENT_TYPE_ITEM;
                break;
            case USER_ID_POSTS:
            case POSTS:
                type = AntiSocialMediaContract.Post.CONTENT_TYPE_LIST;
                break;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }

        return type;
    }

    @Override
    public Uri insert(final Uri uri, final ContentValues values) {
        final SQLiteDatabase db = mDBHelper.getWritableDatabase();

        final int type = URI_MATCHER.match(uri);
        final Uri insertedRow;
        switch (type) {
            case USERS:
                final long userId = db.insertWithOnConflict(AntiSocialMediaContract.User.TABLE_NAME, null,
                                                            values, SQLiteDatabase.CONFLICT_REPLACE);
                insertedRow = AntiSocialMediaContract.User.buildUserUri(userId);
                break;

            case POSTS:
                final long postId = db.insertWithOnConflict(AntiSocialMediaContract.Post.TABLE_NAME, null,
                                                            values, SQLiteDatabase.CONFLICT_REPLACE);
                insertedRow = AntiSocialMediaContract.Post.buildPostUri(postId);
                break;

            case POST_ID:
            case USER_ID:
            case USER_ID_POSTS:
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return insertedRow;
    }

    @Override
    public int delete(final Uri uri, final String selection, final String[] selectionArgs) {
        // TODO: Switch to use SelectionBuilder
        final SQLiteDatabase db = mDBHelper.getWritableDatabase();
        final String finalSelection;
        switch (URI_MATCHER.match(uri)) {
            case POST_ID:
                final StringBuilder selectionBuilder = new StringBuilder(AntiSocialMediaContract.Post._ID + "=");
                final String rowId = uri.getPathSegments().get(1);
                selectionBuilder.append(rowId);
                if (false == TextUtils.isEmpty(selection)) {
                    selectionBuilder
                            .append(" AND (")
                            .append(selection)
                            .append(")");
                }
                finalSelection = selectionBuilder.toString();
                break;
            default:
                // A where clause of some kind is required if we want a deleted count. "where 1" will do
                finalSelection = TextUtils.isEmpty(selection) ? "1" : selection;
        }

        final int deletedCount = db.delete(AntiSocialMediaContract.Post.TABLE_NAME, finalSelection, selectionArgs);

        getContext().getContentResolver().notifyChange(uri, null);

        return deletedCount;
    }

    @Override
    public int update(final Uri uri, final ContentValues values, final String selection, final String[] selectionArgs) {
        final SQLiteDatabase db = mDBHelper.getWritableDatabase();
        final String finalSelection;

        switch (URI_MATCHER.match(uri)) {
            case POST_ID:
                final StringBuilder selectionBuilder = new StringBuilder(AntiSocialMediaContract.Post._ID + "=");
                final String rowId = uri.getPathSegments().get(1);
                selectionBuilder.append(rowId);
                if (false == TextUtils.isEmpty(selection)) {
                    selectionBuilder
                            .append(" AND (")
                            .append(selection)
                            .append(")");
                }
                finalSelection = selectionBuilder.toString();
                break;
            default:
                finalSelection = selection;
        }
        final int updateCount = db.update(AntiSocialMediaContract.Post.TABLE_NAME, values, finalSelection,
                                          selectionArgs);

        getContext().getContentResolver().notifyChange(uri, null);

        return updateCount;
    }

    @Override
    public ContentProviderResult[] applyBatch(final ArrayList<ContentProviderOperation> operations) throws
            OperationApplicationException {
        final SQLiteDatabase db = mDBHelper.getWritableDatabase();
        db.beginTransaction();
        try {
            final int numOperations = operations.size();
            final ContentProviderResult[] results = new ContentProviderResult[numOperations];
            for (int i = 0; i < numOperations; i++) {
                final ContentProviderOperation operation = operations.get(i);
                results[i] = operation.apply(this, results, i);
            }
            db.setTransactionSuccessful();
            return results;
        } finally {
            db.endTransaction();
        }
    }

    /*
     * Privates
     */
    private static final int USERS = 100;
    private static final int USER_ID = 101;
    private static final int USER_ID_POSTS = 102;

    private static final int POSTS = 200;
    private static final int POST_ID = 201;

    private static final UriMatcher URI_MATCHER = buildUriMatcher();

    private AntiSocialMediaDBHelper mDBHelper;

    private static UriMatcher buildUriMatcher() {
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = AntiSocialMediaContract.AUTHORITY;
        matcher.addURI(authority, "users", USERS);
        matcher.addURI(authority, "users/#", USER_ID);
        matcher.addURI(authority, "users/#/posts", USER_ID_POSTS);
        matcher.addURI(authority, "posts", POSTS);
        matcher.addURI(authority, "posts/#", POST_ID);
        return matcher;
    }

    private SelectionBuilder createSelectionBuilder(final Uri uri) {
        final SelectionBuilder selectionBuilder = new SelectionBuilder();

        final int type = URI_MATCHER.match(uri);
        switch (type) {
            case USERS:
                selectionBuilder.table(AntiSocialMediaContract.User.TABLE_NAME);
                break;

            case USER_ID:
                selectionBuilder.table(AntiSocialMediaContract.User.TABLE_NAME)
                                .where(AntiSocialMediaContract.User._ID + "=?", parseUserId(uri));
                break;

            case USER_ID_POSTS:
                selectionBuilder.table(AntiSocialMediaContract.Post.TABLE_NAME)
                                .where(AntiSocialMediaContract.Post.COLUMN_NAME_USER_ID + "=?", parseUserId(uri));
                break;

            case POSTS:
                selectionBuilder.table(Tables.POSTS_JOIN_USERS)
                                .mapToTable(AntiSocialMediaContract.Post._ID, AntiSocialMediaContract.Post.TABLE_NAME);
                break;

            case POST_ID:
                selectionBuilder.table(AntiSocialMediaContract.Post.TABLE_NAME)
                                .where(AntiSocialMediaContract.Post._ID + "=?", parsePostId(uri));
                break;
        }
        return selectionBuilder;
    }

    private String parsePostId(final Uri uri) {
        return uri.getPathSegments().get(1);
    }

    private String parseUserId(final Uri uri) {
        return uri.getPathSegments().get(1);
    }

    private interface Tables {
        public static final String POSTS_JOIN_USERS = AntiSocialMediaContract.Post.TABLE_NAME + " "
                + "INNER JOIN " + AntiSocialMediaContract.User.TABLE_NAME + " ON "
                + AntiSocialMediaContract.Post.COLUMN_NAME_USER_ID + "="
                + AntiSocialMediaContract.User.TABLE_NAME + "." + AntiSocialMediaContract.User._ID;
    }
}

