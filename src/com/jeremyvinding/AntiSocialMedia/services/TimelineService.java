/*
 * This work is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License (http://creativecommons.org/licenses/by-sa/3.0/)
 */
package com.jeremyvinding.AntiSocialMedia.services;

import android.app.IntentService;
import android.content.ContentProviderOperation;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.os.IBinder;
import android.os.RemoteException;
import com.jeremyvinding.AntiSocialMedia.R;
import com.jeremyvinding.AntiSocialMedia.providers.AntiSocialMediaContract;
import com.jeremyvinding.java.common.AutoClosingCallable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import static com.jeremyvinding.android.common.LogUtils.LOGE;

public class TimelineService extends IntentService {
    @SuppressWarnings("UnusedDeclaration")
    public TimelineService() {
        super(TimelineService.class.getSimpleName());
    }

    @SuppressWarnings("UnusedDeclaration")
    public TimelineService(final String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(final Intent intent) {
        try {
            final JSONObject json = makeTotallyRealHttpRequest();
            if (null != json) {
                final ArrayList<ContentProviderOperation> operations = new ArrayList<ContentProviderOperation>();
                parseUsers(operations, json);
                parsePosts(operations, json);
                try {
                    getContentResolver().applyBatch(AntiSocialMediaContract.AUTHORITY, operations);
                } catch (final RemoteException e) {
                    throw new RuntimeException("Problem parsing JSON", e);
                } catch (final OperationApplicationException e) {
                    throw new RuntimeException("Problem parsing JSON", e);
                }
            }
        } catch (final JSONException e) {
            LOGE(TAG, "Error parsing json", e);
        }
    }

    @Override
    public IBinder onBind(final Intent intent) {
        return null;
    }

    // privates
    private static final Object LOCK = new Object();
    private static final String TAG = TimelineService.class.getSimpleName();
    @SuppressWarnings("FieldMayBeFinal") // work around a bug in Idea
    private static boolean sHasUpdated = false;

    private JSONObject makeTotallyRealHttpRequest() throws JSONException {
        JSONObject json = null;
        synchronized (LOCK) {
            if (false == sHasUpdated) {
                final AutoClosingCallable<JSONObject> callable = new AutoClosingCallable<JSONObject>() {
                    @Override
                    protected JSONObject doWork() throws Exception {
                        final InputStream is = autoclose(getResources().openRawResource(R.raw.feed));
                        final BufferedReader reader = autoclose(new BufferedReader(new InputStreamReader(is)));
                        final StringBuilder builder = new StringBuilder();
                        String line;
                        while (null != (line = reader.readLine())) {
                            builder.append(line);
                        }
                        return new JSONObject(builder.toString());
                    }
                };
                try {
                    json = callable.call();
                } catch (final IOException e) {
                    LOGE(TAG, "Error reading json", e);
                } catch (final JSONException e) {
                    throw e;
                } catch (final Exception e) {
                    LOGE(TAG, "Unknown Error", e);
                }
                sHasUpdated = true;
            }
        }
        return json;
    }

    private void parseUsers(final ArrayList<ContentProviderOperation> operations,
                            final JSONObject json) throws JSONException {
        final JSONArray users = json.getJSONArray("users");
        final int length = users.length();
        for (int i = 0; i < length; ++i) {
            final JSONObject user = users.getJSONObject(i);
            final ContentProviderOperation.Builder builder = ContentProviderOperation
                    .newInsert(AntiSocialMediaContract.User.CONTENT_URI)
                    .withValue(AntiSocialMediaContract.User._ID, user.getLong("id"))
                    .withValue(AntiSocialMediaContract.User.COLUMN_NAME_DISPLAY_NAME, user.getString("name"))
                    .withValue(AntiSocialMediaContract.User.COLUMN_NAME_USER_NAME, user.getString("nickname"))
                    .withValue(AntiSocialMediaContract.User.COLUMN_NAME_AVATAR_URL, user.getString("avatar"));
            operations.add(builder.build());
        }
    }

    private void parsePosts(final ArrayList<ContentProviderOperation> operations,
                            final JSONObject json) throws JSONException {
        final JSONArray posts = json.getJSONArray("posts");
        final int length = posts.length();
        for (int i = 0; i < length; ++i) {
            final JSONObject post = posts.getJSONObject(i);
            final ContentProviderOperation.Builder builder = ContentProviderOperation
                    .newInsert(AntiSocialMediaContract.Post.CONTENT_URI)
                    .withValue(AntiSocialMediaContract.Post._ID, post.getLong("id"))
                    .withValue(AntiSocialMediaContract.Post.COLUMN_NAME_USER_ID, post.getLong("userId"))
                    .withValue(AntiSocialMediaContract.Post.COLUMN_NAME_TEXT, post.getString("text"))
                    .withValue(AntiSocialMediaContract.Post.COLUMN_NAME_TIMESTAMP, post.getLong("timestamp"));
            operations.add(builder.build());
        }
    }
}
