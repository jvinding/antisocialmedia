/*
 * This work is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License (http://creativecommons.org/licenses/by-sa/3.0/)
 */
package com.jeremyvinding.AntiSocialMedia.timeline;

import android.app.ListFragment;
import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.jeremyvinding.AntiSocialMedia.R;
import com.jeremyvinding.AntiSocialMedia.activities.UserProfileActivity;
import com.jeremyvinding.AntiSocialMedia.providers.AntiSocialMediaContract;
import com.jeremyvinding.AntiSocialMedia.providers.AntiSocialMediaProvider;
import com.jeremyvinding.AntiSocialMedia.util.ImageLoader;

public class TimelineFragment extends ListFragment implements LoaderManager.LoaderCallbacks<Cursor> {

    public void setUserId(final long userId) {
        mUserId = userId;
    }

    public void loadTimeline() {
        getLoaderManager().initLoader(0, null, this);
    }

    public void setRowLayout(final int layoutId, final String[] projection) {
        mProjection = projection;
        mAdapter = new TimelineAdapter(getActivity(), layoutId);
        setListAdapter(mAdapter);
    }

    @Override
    public Loader<Cursor> onCreateLoader(final int id, final Bundle args) {
        final Uri uri;
        if (mUserId > -1) {
            uri = AntiSocialMediaContract.User.buildPostsUri(mUserId);
        } else {
            uri = AntiSocialMediaContract.Post.CONTENT_URI;
        }
        return new CursorLoader(getActivity(),
                                uri,
                                mProjection,
                                null,
                                null,
                                AntiSocialMediaProvider.ORDER_BY_MOST_RECENT_FIRST);
    }

    @Override
    public void onLoadFinished(final Loader<Cursor> loader, final Cursor data) {
        mAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(final Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }

    /*
     * privates
     */
//    private static final String TAG = TimelineFragment.class.getSimpleName();

    private long mUserId = -1;
    private TimelineAdapter mAdapter;
    private String[] mProjection;

    private static class TimelineAdapter extends CursorAdapter {
        public TimelineAdapter(final Context context, final int layoutId) {
            super(context, null, 0);
            mLayoutId = layoutId;
        }

        @Override
        public boolean isEnabled(final int position) {
            return false;
        }

        @Override
        public View newView(final Context context, final Cursor cursor, final ViewGroup parent) {
            final TimelineViewHolder viewHolder = TimelineViewHolder.get(null, parent, mLayoutId);
            if (null != viewHolder.avatar) {
                final View.OnClickListener userClickListener = new View.OnClickListener() {
                    @Override
                    public void onClick(final View v) {
                        if (mColumnId > -1) {
                            final Intent intent = new Intent(context, UserProfileActivity.class);
                            intent.putExtra(UserProfileActivity.INTENT_KEY_USER_ID, viewHolder.userId);
                            context.startActivity(intent);
                        }
                    }
                };
                viewHolder.avatar.setOnClickListener(userClickListener);
            }
            return viewHolder.root;
        }

        @Override
        public void bindView(final View view, final Context context, final Cursor cursor) {
            final TimelineViewHolder viewHolder = TimelineViewHolder.get(view, null, mLayoutId);
            viewHolder.userId = mColumnId > -1 ? cursor.getLong(mColumnId) : -1;
            if (null != viewHolder.userName && mColumnName > -1) {
                viewHolder.userName.setText(String.format("@%s", cursor.getString(mColumnName)));
            }
            if (null != viewHolder.displayName && mColumnDisplayName > -1) {
                viewHolder.displayName.setText(cursor.getString(mColumnDisplayName));
            }
            if (null != viewHolder.text && mColumnText > -1) {
                viewHolder.text.setText(cursor.getString(mColumnText));
            }
            if (null != viewHolder.timestamp && mColumnTimestamp > -1) {
                final long timestamp = cursor.getLong(mColumnTimestamp);
                final CharSequence relativeTime = DateUtils.getRelativeTimeSpanString(timestamp);
                viewHolder.timestamp.setText(relativeTime);
            }
            if (null != viewHolder.avatar && mColumnAvatar > -1) {
                new ImageLoader(viewHolder.avatar, R.drawable.silhouette).execute(cursor.getString(mColumnAvatar));
            }
        }

        @Override
        public Cursor swapCursor(final Cursor newCursor) {
            findColumns(newCursor);
            return super.swapCursor(newCursor);
        }

        private int mColumnId;
        private int mColumnName;
        private int mColumnDisplayName;
        private int mColumnText;
        private int mColumnTimestamp;
        private int mColumnAvatar;

        private final int mLayoutId;

        private void findColumns(final Cursor cursor) {
            if (null == cursor) {
                mColumnId = -1;
                mColumnName = -1;
                mColumnDisplayName = -1;
                mColumnText = -1;
                mColumnTimestamp = -1;
                mColumnAvatar = -1;
            } else {
                mColumnId = cursor.getColumnIndex(AntiSocialMediaContract.Post.COLUMN_NAME_USER_ID);
                mColumnName = cursor.getColumnIndex(AntiSocialMediaContract.User.COLUMN_NAME_USER_NAME);
                mColumnDisplayName = cursor.getColumnIndex(AntiSocialMediaContract.User.COLUMN_NAME_DISPLAY_NAME);
                mColumnText = cursor.getColumnIndex(AntiSocialMediaContract.Post.COLUMN_NAME_TEXT);
                mColumnTimestamp = cursor.getColumnIndex(AntiSocialMediaContract.Post.COLUMN_NAME_TIMESTAMP);
                mColumnAvatar = cursor.getColumnIndex(AntiSocialMediaContract.User.COLUMN_NAME_AVATAR_URL);
            }
        }

        private static class TimelineViewHolder {
            public final View root;
            public final TextView userName;
            public final TextView displayName;
            public final TextView timestamp;
            public final TextView text;
            public long userId;
            public final ImageView avatar;

            public static TimelineViewHolder get(final View convertView, final ViewGroup parent, final int layoutId) {
                final TimelineViewHolder viewHolder;
                if (null == convertView) {
                    viewHolder = new TimelineViewHolder(parent, layoutId);
                } else {
                    viewHolder = (TimelineViewHolder)convertView.getTag();
                }
                return viewHolder;
            }

            private TimelineViewHolder(final ViewGroup parent, final int layoutId) {
                final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
                root = inflater.inflate(layoutId, parent, false);
                root.setTag(this);
                userName = (TextView)root.findViewById(R.id.user_name);
                displayName = (TextView)root.findViewById(R.id.user_display_name);
                timestamp = (TextView)root.findViewById(R.id.post_timestamp);
                text = (TextView)root.findViewById(R.id.post_text);
                avatar = (ImageView)root.findViewById(R.id.user_avatar);
            }
        }
    }
}
