/*
 * This work is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License (http://creativecommons.org/licenses/by-sa/3.0/)
 */
package com.jeremyvinding.AntiSocialMedia.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;
import com.jeremyvinding.java.common.AutoClosingCallable;

import java.io.IOException;
import java.io.InputStream;

import static com.jeremyvinding.android.common.LogUtils.LOGE;

// NOTE: Do NOT take this class as an example of how to properly load images for ListViews
// there's a race condition if the ListView is scrolled quickly. this is NOT production code.
public class ImageLoader extends AsyncTask<String, Void, Bitmap> {

    public ImageLoader(final ImageView view, final int defaultResourceId) {
        mView = view;
        mDefaultResourceId = defaultResourceId;
    }

    @Override
    protected Bitmap doInBackground(final String... params) {
        Bitmap bitmap = null;
        final String filename = params[0];
        if (null != filename) {
            final AutoClosingCallable<Bitmap> callable = new AutoClosingCallable<Bitmap>() {
                @Override
                protected Bitmap doWork() throws Exception {
                    final InputStream is = autoclose(mView.getContext().getAssets().open(filename));
                    return BitmapFactory.decodeStream(is);
                }
            };
            try {
                bitmap = callable.call();
            } catch (final IOException e) {
                LOGE(TAG, "failed reading in avatar: " + filename, e);
            } catch (final Exception e) {
                LOGE(TAG, "failed reading in avatar: " + filename, e);
            }
        }
        return bitmap;
    }

    @Override
    protected void onPostExecute(final Bitmap bitmap) {
        if (null != bitmap) {
            mView.setImageBitmap(bitmap);
        } else {
            mView.setImageResource(mDefaultResourceId);
        }
    }

    private static final String TAG = ImageLoader.class.getSimpleName();

    private final ImageView mView;
    private final int mDefaultResourceId;
}
